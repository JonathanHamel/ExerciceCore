package com.directmedia.onlinestore.core;

import com.directmedia.onlinestore.core.entity.Artist;
import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.Work;

public class Startup {
    public static void main(String[] args) {
        Artist damienSaez = new Artist("Damien Saez");
        Artist chrisPratt = new Artist("Chris Pratt");
        Artist ironMaiden = new Artist("Iron Maiden");

        Work workDamienSaez = new Work("J'accuse");
        Work workChrisPratt = new Work("Les gardiens de la galaxie");
        Work workIronMaiden = new Work("Dance of death");

        workDamienSaez.setMainArtist(damienSaez);
        workIronMaiden.setMainArtist(ironMaiden);
        workChrisPratt.setMainArtist(chrisPratt);

        workDamienSaez.setGenre("rock");
        workIronMaiden.setGenre("Heavy metal");
        workChrisPratt.setGenre("super-héros");

        workDamienSaez.setRelease(2004);
        workIronMaiden.setRelease(1996);
        workChrisPratt.setRelease(2008);

        workDamienSaez.setSummary("Un résumé");
        workIronMaiden.setSummary("Un résumé");
        workChrisPratt.setSummary("Un résumé");

        Catalogue.listOfWorks.add(workDamienSaez);
        Catalogue.listOfWorks.add(workIronMaiden);
        Catalogue.listOfWorks.add(workChrisPratt);

        for (Work work:Catalogue.listOfWorks) {
            System.out.println(work.getTitle()+ "("+work.getRelease()+ ")");
        }

    }
}
